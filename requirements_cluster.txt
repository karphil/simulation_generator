lxml==4.4.1
numpy==1.17.4
scipy==1.4.1
six==1.12.0
nibabel==3.0.2
